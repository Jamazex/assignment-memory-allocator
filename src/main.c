#include "mem.h"
#include "mem_internals.h"

// Allocating and freeing one block
void test_one() {
    heap_init(REGION_MIN_SIZE);
    printf("TEST 1 START.\n");
    int * x = _malloc(sizeof(int));
    *x = 1;
    printf("x value:%d\n", *x);
    _free(x);
    printf("x value after _free:%d\n", *x);
    printf("TEST 1 END (SUCCESS).\n\n");
}

// Allocating and freeing two blocks, checking for good result
void test_two(){
    heap_init(REGION_MIN_SIZE);
    printf("TEST 2 START.\n");
    int *x = _malloc(sizeof(int));
    *x = 2;
    printf("x value:%d\n", *x);
    char *a = _malloc(sizeof(char)*5);
    for (size_t i = 0; i < 5; i++) a[i] = 'i';
    printf("x value after _malloc string:%d\n", *x);
    printf("a string value:");
    for (size_t i = 0; i < 5; i++) printf("%c", *(a+i));
    printf("\n");
    _free(x);
    printf("x value after _free:%d\n", *x);
    printf("a string value after x freeing:");
    for (size_t i = 0; i < 5; i++) printf("%c", *(a+i));
    printf("\n");
    _free(a);
    printf("a string value after _free:");
    for (size_t i = 0; i < 5; i++) printf("%d", *(a+i));
    printf("\n");
    printf("TEST 2 END (SUCCESS).\n\n");
}

// Allocating two blocks, but second needs to
// allocate another region after previous
void  test_three(){
    heap_init(REGION_MIN_SIZE);
    printf("TEST 3 START.\n");
    int *x = _malloc(sizeof(int));
    *x = 3;
    printf("x value:%d\n", *x);
    char *a = _malloc(sizeof(char )*(REGION_MIN_SIZE+1));
    for (size_t i = 0; i < sizeof(char )*(REGION_MIN_SIZE+1); i++) a[i] = 'i';
    printf("First symbol:%c\n", a[0]);
    printf("Last symbol:%c\n", a[sizeof(char )*(REGION_MIN_SIZE+1)-1]);
    _free(a);
    printf("First symbol after _free:%d\n", a[0]);
    printf("Last symbol after _free:%d\n", a[sizeof(char )*(REGION_MIN_SIZE+1)-1]);
    printf("x value after a freeing:%d\n", *x);
    _free(x);
    printf("x value after _free:%d\n", *x);
    printf("TEST 3 END (SUCCESS).\n\n");
}

// Allocating two blocks, second needs to
// allocate new region, but place after
// previous region unavailable
void test_four(){
    heap_init(REGION_MIN_SIZE);
    printf("TEST 4 START.\n");
    char *a = _malloc(sizeof(char )*(REGION_MIN_SIZE- offsetof(struct block_header, contents)));
    for (size_t i = 0; i < sizeof(char )*(REGION_MIN_SIZE- offsetof(struct block_header, contents)); i++) a[i] = 'i';
    mmap( (void*)(a+REGION_MIN_SIZE - offsetof(struct block_header, contents)), sizeof(char )*(5), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS , 0, 0 );
    int *x = _malloc(sizeof(int)*5);
    *x = 20;
    printf("x value:%d\n", *x);
    printf("First symbol of a:%c\n", a[0]);
    printf("Last symbol of a:%c\n", a[sizeof(char )*(REGION_MIN_SIZE-offsetof(struct block_header, contents))-1]);
    _free(a);
    printf("First symbol after _free:%d\n", a[0]);
    printf("Last symbol after _free:%d\n", a[sizeof(char )*(REGION_MIN_SIZE-offsetof(struct block_header, contents))-1]);
    printf("x value after a freeing:%d\n", *x);
    _free(x);
    printf("x value after _free:%d\n", *x);
    printf("TEST 4 END (SUCCESS).\n\n");
}

int main(){
    test_four();
    return 0;
}

